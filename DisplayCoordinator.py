from AnimationBase import Animation
import numpy as np
from math import floor,ceil
from neopixel import *
from time import sleep
import queue
import spi_reader as reader

#FIXME: Certain combinations of leds at the same value result in flickering - consider a full sweep of colors to find flickery values and correct these to approximate colors?
#FIXME: If any two values = 255, flashing will occur. Don't allow this - bias one way.
#IDEA: Probably add a flag for shitty-ass LEDS, or maybe a correction factor function for each color?
#TODO: to enqueue, take animation class (not instance!) and *args, only instantiate animation when it is about to be displayed.
class DisplayCoordinator:
	def __init__(self, display_size : tuple, display_correction = (), pin = 10, freq_hz = 800000, dma_ch = 5, invert = False, default_brightness = 255, led_ch = 0, strip_type = ws.WS2811_STRIP_GRB):
		self._led_count = 1
		for i in display_size:
			self._led_count *= i
		self._brightness = default_brightness
		self._display_dims = display_size
		self._display_order = len(display_size)
		self._display_corrections = display_correction
		self._broken_range = [
			(1, 6), (11, 16), (21, 26), (29, None), (31, 36), (39, None), (41, 47), (49, None), (51, 53), (56, 57),
			(59, None), (62, 67), (69, 70), (72, 77), (79, 80), (82, 87), (90, None), (92, 98), (100, None), (102, 108),
			(113, 116), (118, None), (120, None), (123, 131), (133, 138), (141, None), (143, 149), (151, None),
			(153, 159),
			(161, None), (164, 169), (171, 179), (181, 182), (184, 189), (192, None), (194, 199), (202, None),
			(204, 210),
			(212, None), (214, 220), (222, None), (225, 229), (230, None), (232, 240), (242, 243), (245, 249),
		]

		self._display_map = np.arange(self._led_count).reshape(display_size)
		##CORRECT FOR DISPLAY IRREGULARITIES##
		for i in range(self._display_order):
			if self._display_corrections[i] is not None:
				for j in range(self._display_dims[i]):
					corrected_matrix = self._display_corrections[i](self._display_map[j], j)
					self._display_map[j] = corrected_matrix

		strip = Adafruit_NeoPixel(self._led_count, pin, freq_hz, dma_ch, invert, self._brightness, led_ch, strip_type)
		self._strip = strip
		strip.begin()
		self._queue = queue.Queue()
		self._current_anim = Animation()

	#begin displaying animations in the queue
	def _display(self):
		while(~self._queue.empty()):
			self._current_anim, anim_reps = self._queue.get()
			self.enqueue(self._current_anim, anim_reps) #to make infinite
			for i in range(anim_reps):
				while(not self._current_anim.done()):
					self._strip.setBrightness(reader.read(0)*255) #TODO replace with ADCreader for trimpot
					self._tick_animation()
					sleep(self._current_anim._wait_ms/1000) #or pause until input
				self._current_anim.reset()

	def _tick_animation(self):
		#print("ticking animation - DC")
		self._current_anim.tick()
		self._next_frame = self._current_anim._next_frame.copy()  # DC can cut/extend frames, so I'm making a copy now
		self._prepare_for_display()
		#print("After prepare, frame is", self._next_frame)#,
		#	  "\ndisplay map is", self._display_map) #REMOVE
		for index,color in zip(self._display_map.flat, self._next_frame.flat):  #actually update pixels
			#print(index,": ",color) #REMOVE
			self._strip.setPixelColor(index.item(), color.item())
		self._strip.show()

	def _prepare_for_display(self): #Cut/extend frame based on display size and dimension
		##CUT ANIMATION##
		#print("preparing animation of size", self._current_anim._anim_dims, "for display of size", self._display_dims,
		#		"\n Frame was", self._next_frame) #REMOVE
		#TODO: Initially handle animations with dimensions not matching the display - the frame buffer will need to be corrected
		display_slicers = ()
		for dim,margins in enumerate(self.calc_margins(self._current_anim._anim_dims)):
			display_slicers += (slice(margins[0], self._current_anim._anim_dims[dim] - margins[1]),) #We prefer to bias to the left of a display, maybe we can do this only if we need to slice, save diffs tuple for this
		#print("slicers are", display_slicers) #REMOVE
		self._next_frame = self._next_frame[display_slicers]
		#print("Frame is now", self._next_frame) #REMOVE

		##CORRECT COLORS##
		#Some color combinations don't work

	def enqueue(self, anim, reps = 1):
		self._queue.put((anim, reps))

	def calc_margins(self, reference_dims):
		"""Will return the margins of your display - the reference dimension"""
		margins = ()
		for dim_ref,dim_display in zip(reference_dims, self._display_dims):
			#print("Reference dimension is", dim_ref, "display dim is", dim_display) #REMOVE
			cur_diff = dim_display - dim_ref
			#print("the difference was",cur_diff) #REMOVE
			#print("diff/2 =", cur_diff/2) #REMOVE
			#print("floor:", math.floor(cur_diff/2), "\nceil:", math.ceil(cur_diff/2)) #REMOVE
			margins += ((int(floor(cur_diff/2)), int(ceil(cur_diff/2))),)
			#print("adding new margin", int(math.floor(cur_diff/2)), int(math.ceil(cur_diff/2))) #REMOVE
		# If difference > 0, animation is bigger than display and needs to be cut
		# if difference < 0, display is bigger than animation and needs to be accounted for FIXME Not allowable currently
		# Display nothing, extend colors on edge, or scale and cut to correct
		return margins

	def __insert_frame(self, frame : np.ndarray):
		restore_frame = self._next_frame
		self._next_frame = frame
		self._prepare_for_display()
		for index,color in zip(self._display_map.flat, self._next_frame.flat):  #actually update pixels
			#print(index,": ",color) #REMOVE
			self._strip.setPixelColor(index.item(), color.item())
		self._next_frame = restore_frame
		self._strip.show()


from ADCReader import ADCReader

class DebugCoordinator(DisplayCoordinator):
	def __init__(self, display_size : tuple, display_correction = (), debug = False, pause_tick = False, pause_anim = False):
		self._debug = debug
		self._pause_tick = pause_tick
		self._pause_anim = pause_anim
		self._real_display_dims = display_size
		self._brightness_feed = ADCReader(0)
		super().__init__(display_size, display_correction, default_brightness=100)

	def _display(self):
		while(~self._queue.empty()):
			self._current_anim, anim_reps = self._queue.get()
			if(self._debug): print("displaying ", self._current_anim, anim_reps, " times")
			self.enqueue(self._current_anim, anim_reps)  # to make infinite
			for i in range(anim_reps):
				while not self._current_anim.done():
					self._strip.setBrightness(self.getBrightness())
					self._tick_animation()
					sleep(self._current_anim._wait_ms / 1000)  # or pause until input
					if(self._pause_tick): input("Enter to tick...")
				self._current_anim.reset()
			if(self._pause_anim): input("Enter to advance queue...")

	def emulate(self, new_size): #FIXME
		if self._debug: print("emulating display of size", new_size)
		slicers = ()
		for margins in self.calc_margins(new_size):
			slicers += (slice(margins[0], margins[1]),)
		if self._debug: print("your slicers today are",slicers)
		self._display_map = self._display_map[slicers]
		self._display_dims = new_size
		print("Emulated display map is",self._display_map)

	def getBrightness(self):
		value = int(self._brightness_feed._value*255)
		return value if value>0 else self._brightness