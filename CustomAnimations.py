from AnimationBase import *
from math import floor,ceil
from random import randint

class Fill_Display(StaticAnimation):
	def __init__(self, color = Color(0, 0, 0)):
		#anim_dims = (Animation.DIMS.INFINITE, Animation.DIMS.INFINITE)
		anim_dims = (9,9)
		num_frames = 1
		frames = np.zeros((num_frames,) + (9, 9), int) #FIXME DisplayCoordinator can't deal with (potential) infinity yet
		frames.fill(color)
		super().__init__(frames)

class IterativeTest(Animation):
	def __init__(self):
		anim_dims = (9, 9)
		super().__init__(anim_dims)
		self._next_frame = np.zeros(anim_dims, int) #TODO see if necessary

	def tick(self):
		self._next_frame[floor(self._frame / 9) % 9][self._frame % 9] = Color(255, 255, 255)
		if (self._frame > 0):
			self._next_frame[floor(self._frame / 9 % 9)][self._frame % 9 - 1] = Color(0, 0, 0)
		if(self._frame % 9 == 0):
			self._next_frame[floor(self._frame / 9 % 9) - 1][8] = Color(0, 0, 0)
		if (self._frame == 0):
			self._next_frame[8][8] = Color(0, 0, 0)
		super().tick()

	def done(self):
		return self._frame == 81

class StaticTest(StaticAnimation):
	def __init__(self):
		num_frames = 10
		anim_dims = (9, 9)
		frames = np.zeros((num_frames,) + anim_dims, int)
		colors = [
			Color(150, 255, 75),
			Color(255, 0, 0),
			Color(175, 20, 50),
			Color(50, 50, 100),
			Color(99, 100, 32),
			Color(20, 30, 50),
			Color(66, 66, 66),
			Color(77, 77, 77),
			Color(1, 2, 3),
			Color(69, 420, 71)
		]
		for i in range(num_frames):
			color = colors[i]
			frames[i].fill(color)
		super().__init__(frames)

class ColorWipe1D(Animation):
	def __init__(self, colors : tuple):
		anim_dims = (9, 9)
		self._colors = colors
		self._iter = 0
		super().__init__(anim_dims)
		self._next_frame = np.zeros(anim_dims, int)

	def tick(self):
		color = self._colors[self._iter]
		if self._frame % 81 == 80:
			self._iter += 1
		for i in range(self._frame%81 + 1):
			self._next_frame[floor((i%81)/9)][i%9] = color
		super().tick()

	def done(self):
		return self._frame == len(self._colors)*81

	def reset(self):
		self._iter = 0
		super().reset()

class ColorWipe2D(Animation):
	def __init__(self, colors : tuple):
		anim_dims = (9, 9)
		self._colors = colors
		self._iter = 0
		super().__init__(anim_dims)
		self._next_frame = np.zeros(anim_dims, int)

	def tick(self):
		color = self._colors[self._iter]
		if self._frame % 9 == 8:
			self._iter += 1
		for i in range(self._frame%9 + 1):
			self._next_frame[:][i] = color
		super().tick()

	def done(self):
		return self._frame == len(self._colors)*self._anim_dims[1]

	def reset(self):
		self._iter = 0
		super().reset()


class Gradient1D(StaticAnimation):
	def __init__(self, color1, color2):
		num_frames = 1
		anim_dims = (9, 9)

		frames = np.zeros((num_frames,) + anim_dims, int)
		frames[0][:] = np.linspace(color1, color2, anim_dims[0])
		super().__init__(frames)

class ColorSpiral(Animation): #FIXME selecting index doesn't work
	def __init__(self):
		anim_dims = (9, 9)
		super().__init__(anim_dims)
		self._next_frame = np.zeros(anim_dims, int)
		#this info will later be provided by DC
		self._traverseRow = True
		self._traverseCol = False
		self._dir = 1
		self._curRow = 0
		self._curCol = 0
		colors = [
			np.linspace(Color(255,0,0), Color(0,255,0), int(81/3)),
			np.linspace(Color(0,255,0), Color(0,0,255), int(81/3)),
			np.linspace(Color(0,0,255), Color(255, 0, 0), int(81/3))
		]
		self._colors = np.zeros((81,), int)
		for i in range(3):
			for j in range(27):
				self._colors[i*27+j] = colors[i][j]
		print("colors are ", self._colors)

	def tick(self):
		print("frame is", self._frame)
		print("updating index", self._curRow, ",", self._curCol)
		self._next_frame = np.zeros(self._anim_dims, int)
		self._next_frame[self._curCol][self._curRow] = self._colors[self._frame]
		if(self._frame % 9 == 8):
			self._traverseRow = not self._traverseRow
			self._traverseCol = not self._traverseCol
		if(self._frame % 17 == 16):
			self._dir *= -1
		if self._traverseRow:
			#print("Traversing row in",self._dir,"dir")
			self._curRow += self._dir
		if self._traverseCol:
			#print("Traversing col in", self._dir, "dir")
			self._curCol += self._dir
		super().tick()

	def done(self):
		return self._frame == 81

class RandomTrail(Animation):
	def __init__(self, trail_length = 10):
		anim_dims = (9, 9)
		super().__init__(anim_dims, wait_ms=100)
		self._row = 0
		self._col = 0
		#self._trail_length = trail_length
		self._num_frames = 100
		#self._trail = queue.Queue(10)

	def tick(self):
		nextColor = Color(randint(0, 255),randint(0, 255),randint(0, 255))
		#move
		dir = randint(0, 4)
		if dir == 0:
			self._col += 1
		elif dir == 1:
			self._row += 1
		elif dir == 2:
			self._col -= 1
		else:
			self._row -= 1
		#check for overflow
		if self._row > 8:
			self._row = 0
		if self._col > 8:
			self._col = 0
		if self._row < 0:
			self._row = 8
		if self._col < 0:
			self._col = 8
		self._next_frame = np.zeros(self._anim_dims, int)
		self._next_frame[self._col][self._row] = nextColor
		super().tick()

	def done(self):
		return self._frame == self._num_frames

class MinesLogo(StaticAnimation):
	def __init__(self, color_bg = Color(33, 49, 255), color_M = Color(255, 255, 255)):
		anim_dims = (9,9)
		num_frames = 1
		#color_bg = Color(33, 49, 77) #mines blue
		minesM = np.zeros((9,9))
		minesM.fill(color_bg)
		minesM[1:8,1] = color_M
		minesM[1:8,7] = color_M
		minesM[6,2] = color_M
		minesM[5,3] = color_M
		minesM[4,4] = color_M
		minesM[5,5] = color_M
		minesM[6,6] = color_M
		frames = np.ndarray((num_frames,) + anim_dims, int)
		frames[0] = minesM
		super().__init__(frames)