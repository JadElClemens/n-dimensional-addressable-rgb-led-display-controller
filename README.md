#Multidimensional Addressable RGB LED Display Controller for Raspberry Pi
Simple (I think) controller for ws281x-style LEDs based on the neopixel python library from jgarff's rpi_ws281x library.

Currently mostly limited to 2d and pretty much hardcoded for 9x9 display size.

Presently doesn't handle threading very well, quitting the program can take a fair few ^Cs

[`DisplayCoordinator`](DisplayCoordinator.py) is capable of cutting too-big animations down to size for the display, but will fail when the animation is too small.
Keep this in mind for now when creating custom animations.


Basic Usage
-
```python
from DisplayCoordinator import DisplayCoordinator
from AnimationBase import *
```

Create a child of [`Animation`](Animation.py) or [`StaticAnimation`](Animation.py) - StaticAnimation is just a convenience class for
statically-defined animations with a set number of frames.

If subclassing `Animation`, you must override `tick()` so it fills out the animation's next frame. Obviously, this can be based on
anything - `Animation` has a built-in frame counter (`self._frame`) which is useful.

If subclassing `StaticAnimation`, all you need to do is create a np.ndarray of shape ((num_frames,) + anim_dims), fill out these frames in `__init__`,
and call `super().init(frames)` - the animation dimensions and number of frames will be inferred from the shape of `frames`.

For development, it is probably useful to use `DebugCoordinator` from [DisplayCoordinator.py](DisplayCoordinator.py). The constructor
has flags for debug (will print the currently running animation), pause tick (pause after ticking animation and updating LEDs),
and pause animation (pause after an animation has finished all of its repetitions - will fix to pause after each rep).

Overview
-
####Animation
An `Animation` represents a programmed sequence that can be displayed on an addressable LED display.

Its constructor accepts a tuple of dimensions `anim_dims` and a frame-to-frame wait time in ms, `wait_ms`.
It has a frame buffer, `_next_frame`, that is assumed (but not yet enforced) to be the same shape as `_anim_dims`

####DisplayCoordinator
The `DisplayCoordinator` represents a single LED display of any order. 

Most crucially, the constructor accepts a tuple of display dimensions `display_size` and a tuple of display correction functions `display_correction`.
This is not currently asserted, but these parameters should be the same size.

In `__init__`, a display map is created that represents a mapping of the frame array positions to the LED indices in the strip. 
This is constructed using `np.arange(num_leds).reshape(display_size)` - for this reason, you should put your display dimensions in the
order that you would like to address them when updating frames.

`display_correction` should contain correction functions for display irregularities which should each accept a `np.ndarray` and an int as parameters.
The np.ndarray is the "slice" of the display map in the axis that is being considered for correction - if a certain axis does not need
to be corrected in any way, use `None` as the argument for this axis.

The constructor also takes arguments for every argument that the `Adafruit_NeoPixel` constructor accepts with the exception of `num` for the numer of LEDs - this is calculated from `display_size`.

After creating an instance of `DisplayCordinator`, add Animation instances to the display queue by calling `enqueue(animation, num_reps)`.
Once everything is in the queue, call `_display()` to begin the display loop - the queue currently loops infinitely, but even when
animations were actually removed from the queue, `_display()` would never exit (or, at least, the program wouldn't).

Hopes & Dreams
-
* `DisplayCoordinator` will be mature enough to handle `Animation`s which are a different size than the display in a given dimension
(although for many) animations, automatic extension to display size could be trivial)
* `DisplayCoordinator` can deal with `Animation`s which have a different order than the display
* `Animation`(AnimationBase.py)s are aware of context and can access display size
    * This can be used to create animations that can display on any given pixel (e.g. random movement) or rely on
    some signal for when the edge of a dimension is reached (e.g. a color wipe)
* Better multithreading (namely, displaying frame should be in a separate thread operating constantly, and 
some sort of lock should be used when DC needs to update the display frame, conflicts with setPixelColor and show loop)
