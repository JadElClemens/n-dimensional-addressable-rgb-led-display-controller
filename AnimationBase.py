from neopixel import *
import numpy as np

#IDEA overlay animation on animation (select overlaid display offset+dims, no reason you couldn't have multiple overlays), CompoundAnimation((Anim, (new_dims,)) maybe?
#IDEA Add "Effects" like downsize blend modes,
#look into pillow library for image interpretation
class Animation:
	def __init__(self, anim_dims = (), wait_ms = 50):
		self._frame = 0
		self._next_frame = np.ndarray(anim_dims, int)
		self._anim_dims = anim_dims
		self._anim_order = len(anim_dims)
		self._wait_ms = wait_ms

	#perform one animation tick. Should the animation scheduler handle waiting or the animation? Probably the scheduler
	def tick(self):
		self._frame += 1

	def done(self):
		pass

	def reset(self):
		self._frame = 0

	def on_traverse_dim(self, dim : int):
		pass

	class DIMS:
		INFINITE = 0

class StaticAnimation(Animation):
	#Might accept a tuple of dimensions later, not super relevant now
	def __init__(self, frames : np.ndarray, wait_ms = 200):
		self._num_frames = frames.shape[0]
		self._frames = frames
		#if(frames.ndim - 1 != anim_dims): #frames will have one more dimension - matches anim_dims plus a dimension for each frame
		#	pass
		super().__init__(frames.shape[1:], wait_ms)

	def tick(self):
		self._next_frame = self._frames[self._frame] #assign next frame
		super().tick() #just advances frame # for now

	def done(self):
		return self._frame == self._num_frames