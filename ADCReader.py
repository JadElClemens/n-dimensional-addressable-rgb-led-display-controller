import threading
import spi_reader as reader
import RPi.GPIO as gpio
from time import sleep

class ADCReader:
	def __init__(self, channel):
		self._value = 0
		self._channel = channel
		threading.Thread(target=self.continualRead).start()

	def continualRead(self):
		while(True):
			self._value = reader.read(self._channel)/1024.0
			sleep(0.25)

class FanController:
	def __init__(self, fan_pin):
		self._reader = ADCReader(1) #reads temp
		self._pin = fan_pin
		self._status = False
		threading.Thread(target=self.controlFans).start()

	def controlFans(self):
		gpio.setup(self._pin, gpio.OUT)
		count = 1
		while(True):

			millivolts = self._reader._value / 1024.0
			# 10 mv per degree
			temp_C = millivolts * 100

			if temp_C > 0 and count > 0:
				if temp_C <= 9:
					count = 10
				count -= 1
				if not self._status:
					gpio.output(self._pin, gpio.HIGH)
					self._status = True
			else:
				count = 1
				if self._status:
					gpio.output(self._pin, gpio.LOW)
					self._status = False
			sleep(0.25)